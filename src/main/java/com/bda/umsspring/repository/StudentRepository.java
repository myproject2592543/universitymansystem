package com.bda.umsspring.repository;

import com.bda.umsspring.entity.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface StudentRepository extends JpaRepository<StudentEntity, Long>, CrudRepository<StudentEntity, Long> {
}
