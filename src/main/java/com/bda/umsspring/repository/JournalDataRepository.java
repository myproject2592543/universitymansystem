package com.bda.umsspring.repository;

import com.bda.umsspring.entity.JournalDataEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JournalDataRepository extends JpaRepository<JournalDataEntity, Long> {
}
