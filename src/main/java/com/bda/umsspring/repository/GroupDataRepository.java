package com.bda.umsspring.repository;

import com.bda.umsspring.entity.GroupDataEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupDataRepository extends JpaRepository<GroupDataEntity, Long> {
}
