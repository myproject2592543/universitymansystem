package com.bda.umsspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniversityManSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(UniversityManSystemApplication.class, args);
	}

}
