package com.bda.umsspring.controller;

import com.bda.umsspring.entity.GroupDataEntity;
import com.bda.umsspring.service.GroupDataServices;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class GroupDataController {
    @Autowired
    GroupDataServices groupDataServices;

    @GetMapping("/groupdata")
    public List<GroupDataEntity> getGroupDatas(){
        return groupDataServices.getGroupDatas();
    }

    @PostMapping("/admin/add-teacher")
    public void addTeacherToGroup(){
        groupDataServices.createGroupData();
    }

}
