package com.bda.umsspring.service;

import com.bda.umsspring.entity.JournalDataEntity;

import java.util.List;

public interface JournalDataServices {
    void createJournalData();
    List<JournalDataEntity> getJournalDatas();
}
