package com.bda.umsspring.service;

import com.bda.umsspring.entity.StudentEntity;

import java.util.List;

public interface StudentServices {

    void createStudent();
    List<StudentEntity> getStudents();
}
