package com.bda.umsspring.service;

import com.bda.umsspring.entity.JournalEntity;

import java.util.List;

public interface JournalServices {
    void createJournal();
    List<JournalEntity> getJournals();
}
