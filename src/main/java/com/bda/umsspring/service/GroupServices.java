package com.bda.umsspring.service;

import com.bda.umsspring.entity.GroupEntity;

import java.util.List;

public interface GroupServices {
    void createGroup();
    List<GroupEntity> getGroups();
}
