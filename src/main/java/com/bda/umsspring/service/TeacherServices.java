package com.bda.umsspring.service;

import com.bda.umsspring.entity.TeacherEntity;

import java.util.List;

public interface TeacherServices {
    void createTeacher();
    List<TeacherEntity> getTeachers();
}
