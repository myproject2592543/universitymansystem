package com.bda.umsspring.service;

import com.bda.umsspring.entity.GroupDataEntity;

import java.util.List;

public interface GroupDataServices {
    void createGroupData();
    List<GroupDataEntity> getGroupDatas();
}
