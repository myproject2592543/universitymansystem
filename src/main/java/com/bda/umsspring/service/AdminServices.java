package com.bda.umsspring.service;

import com.bda.umsspring.entity.AdminEntity;

import java.util.List;

public interface AdminServices {


    void createAdmin();
    List<AdminEntity> getAdmins();


}
