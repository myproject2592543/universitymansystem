package com.bda.umsspring.service.impl;


import com.bda.umsspring.entity.AdminEntity;
import com.bda.umsspring.repository.AdminRepository;
import com.bda.umsspring.repository.UserRepository;
import com.bda.umsspring.service.AdminServices;
import com.bda.umsspring.service.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Scanner;

@Service
public class AdminServicesImpl implements AdminServices {
  static   Scanner scanner = new Scanner(System.in);

    @Autowired
    AdminRepository adminRepository;



    @Autowired
    UserRepository userRepository;

    @Autowired
    UserServices userServices;




    public void createAdmin(){
        System.out.println("-------------------------");
        System.out.println("Admin elave edilmesi");
        System.out.println("-------------------------");
        System.out.println("name:");
        String name = scanner.next();
        System.out.println("surname:");
        String surname = scanner.next();
        System.out.println("username:");
        String username = scanner.next();
        System.out.println("password:");
        String password = scanner.next();
        Long roleId = 1L;

        AdminEntity admin = new AdminEntity(null,name,surname,username,password,roleId);
        adminRepository.save(admin);
        userServices.createUser(name,surname,username,password,roleId);

    }

    public List<AdminEntity> getAdmins(){
        return adminRepository.findAll();
    }







}
